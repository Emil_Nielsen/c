#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include <iostream>
#include "opencv2/opencv.hpp"
using namespace std;
using namespace cv;

Mat cap_gray;
int thresh = 100;
RNG rng(12345);
void thresh_callback(int, void*);

int main() {


    VideoCapture cap("C:/Users/Emil Nielsen/OpenCV/opencv videos/coins.mp4");

    int frame_width = cap.get(cv::CAP_PROP_FRAME_WIDTH);
    int frame_height = cap.get(cv::CAP_PROP_FRAME_HEIGHT);

    VideoWriter video("C:/Users/Emil Nielsen/OpenCV/opencv videos/Blob.api", VideoWriter::fourcc('M', 'J', 'P', 'G'), 25, Size(frame_width, frame_height));

    while (1) {

        Mat frame;
        cap >> frame;
        video.write(frame);
        cvtColor(frame, cap_gray, COLOR_BGR2GRAY);
        blur(cap_gray, cap_gray, Size(3, 3));

        imshow("Frame", frame);

        thresh_callback(0, 0);
        char c = (char)waitKey(1);
        if (c == 27)
            break;
    }

    cap.release();
    return 0;
}

void thresh_callback(int, void*)
{
    Mat canny_output;
    Canny(cap_gray, canny_output, thresh, thresh * 2);
    vector<vector<Point> > contours;
    findContours(canny_output, contours, RETR_TREE, CHAIN_APPROX_SIMPLE);
    vector<vector<Point> > contours_poly(contours.size());
    vector<Rect> boundRect(contours.size());
    vector<Point2f>centers(contours.size());
    vector<float>radius(contours.size());
    for (size_t i = 0; i < contours.size(); i++)
    {
        approxPolyDP(contours[i], contours_poly[i], 3, true);
        boundRect[i] = boundingRect(contours_poly[i]);
        minEnclosingCircle(contours_poly[i], centers[i], radius[i]);
    }
    Mat drawing = Mat::zeros(canny_output.size(), CV_8UC3);
    for (size_t i = 0; i < contours.size(); i++)
    {
        Scalar color = Scalar(rng.uniform(0, 256), rng.uniform(0, 256), rng.uniform(0, 256));
        drawContours(drawing, contours_poly, (int)i, color);
        //rectangle(drawing, boundRect[i].tl(), boundRect[i].br(), color, 2);
        circle(drawing, centers[i], (int)radius[i], color, 2);
    }
    imshow("Blobs", drawing);
}