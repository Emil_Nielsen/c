/*#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include <opencv2/features2d.hpp>
#include <iostream>
#include "opencv2/imgcodecs.hpp"
#include <iomanip>


using namespace cv;
using namespace std;

int circle_detector(Mat src) {
	medianBlur(src, src, 5);	// Median blur with kernel size of 5
	vector<Vec3f> circles;		// Vector of 3 floating point integers	
	HoughCircles(src, circles,  // circles: A vector that stores sets of 3 values: xc,yc,r for each detected circle.
		HOUGH_GRADIENT,// Detection method - HOUGH_GRADIENT is the only one as of OpenCV 4.6.0
		1,				// Inverse ratio of resolution 
		src.rows / 16,   // Minimum distance in pixels between the circles
		100,			// Upper threshold for internal Canny edge detector
		30,			// Threshold for center detection
		20,				// Minimum radius to be detected. If unknown, set to zero.
		70				// Maximum radius to be detected. If unknown, set to zero.
	);


	for (size_t i = 0; i < circles.size(); i++)
	{
		Vec3i c = circles[i];	// Convert circles vector to hold integers.
		Point center = Point(c[0], c[1]); // Define center points as Point object
		circle(src, center, 5, Scalar(128, 0, 128), 3, LINE_AA); // Define center dot size and color.
		int radius = c[2]; // Find radius of enclosing circles
		circle(src, center, radius, Scalar(255, 0, 255), 3, LINE_AA); // Draw gray circles around detected circles
	}
	imshow("Detected circles", src);
	waitKey();

	return circles.size();
}
Mat src_gray;
int thresh = 75;
RNG rng(12345);
void thresh_callback(int, void*);

using namespace cv;
using namespace std;
int main(int argc, char** argv) {
	Mat src = imread("C:/Users/Emil Nielsen/OpenCV/opencv images/coins_no.png", IMREAD_GRAYSCALE);
	int Numbers = circle_detector(src);
	cout << "Amount of coins: " << Numbers;
}
*/